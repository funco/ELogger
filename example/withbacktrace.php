<?php
/**
 * Created by PhpStorm.
 * User: linhongzhao
 * Date: 18-5-12
 * Time: 上午1:36
 */
require_once __DIR__.'/../vendor/autoload.php';

function a() {
    b();
}

function b() {
    c();
}

function c() {
    $logger = new \EFrame\Logger();
    $logger->trace()->log(\Psr\Log\LogLevel::DEBUG, 'Good!this is your first debug message!');
}

a();
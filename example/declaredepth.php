<?php
/**
 * Created by PhpStorm.
 * User: linhongzhao
 * Date: 18-5-12
 * Time: 上午1:43
 */
require_once __DIR__.'/../vendor/autoload.php';

function a() {
    b();
}

function b() {
    c();
}

function c() {
    $logger = new \EFrame\Logger();
    $logger->trace(2)->log(\Psr\Log\LogLevel::DEBUG, 'Good!this is your first debug message!');
}

a();
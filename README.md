# ELogger

#### Description
+ 日志记录器Logger，遵循PSR-3 规范
+ 在psr\logger基础上完善log方法，以实现较规范和通用的logger
+ logger的写操作通过file_put_content函数，含参数LOCK_EX，在多进程环境下可靠
